package FoodChain.entity;

import FoodChain.foodStuff.StateType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Class containing various tests with respect to Entity assuring that the code is valid
 */
public class entityTest {

    Entity farmer, buyer;

    @Before
    public void setup() {
        farmer = new FarmerEntity(null, 40, new HustleFarmer(StateType.FARMED));
        buyer = new CustomerEntity(null, 100, new BasicCustomer());
    }

    @Test
    public void testCorrectMoneyTransfer() {
        buyer.getBlockchainClient().getWallet().transferMoneyTo(farmer, 40);
        Assert.assertEquals(buyer.getBlockchainClient().getWallet().getMoneyBalance(), 60, 0.01);
        Assert.assertEquals(farmer.getBlockchainClient().getWallet().getMoneyBalance(), 80, 0.01);
    }



}
