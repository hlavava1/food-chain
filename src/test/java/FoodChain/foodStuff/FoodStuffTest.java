package FoodChain.foodStuff;

import FoodChain.blockchain.Block;
import FoodChain.entity.Entity;
import FoodChain.entity.EntityFactory;
import FoodChain.entity.HustleFarmer;
import FoodChain.operations.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Class containing various tests with respect to Food Stuff assuring that the code is valid
 */
public class FoodStuffTest {

    FoodStuff wheat;
    EntityFactory factory;

    @Before
    public void setup() {
        wheat = new Wheat();
        factory = new EntityFactory(null);
    }

    @Test
    public void testCorrectProcessingFlow() {
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.FARMED);
        wheat.process();
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.CLEANED);
        wheat.process();
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.PACKAGED);
        wheat.process();
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.CERTIFIED);
        wheat.process();
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.FINAL);
        wheat.process();
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.FINAL);
    }

    @Test
    public void testCloning() {
        wheat.process();
        FoodStuff wheatClone = wheat.clone();

        Assert.assertEquals(wheat.getFoodState().getType(), wheatClone.getFoodState().getType());

        wheatClone.process();
        Assert.assertEquals(wheat.getFoodState().getType(), StateType.CLEANED);
        Assert.assertEquals(wheatClone.getFoodState().getType(), StateType.PACKAGED);
    }

    @Test
    public void testFoodReport() {

        Entity farmer = factory.makeVegFarmer(100); // new Entity(null, 10, new HustleFarmer(StateType.FARMED));
        Entity reviewer = factory.makeDistributor(100);

        Block blockFarmed = new Block(null, null, null, new FarmOperation(farmer, farmer, wheat), 0);
        Block blockCleaned = new Block(blockFarmed, blockFarmed.getHash(), blockFarmed, new CleanOperation(farmer, farmer, wheat.clone().process()), 1);
        Block blockPackaged = new Block(blockCleaned, blockCleaned.getHash(), blockCleaned, new PackageOperation(farmer, farmer, wheat.clone().process()), 2);
        Block blockCertified = new Block(blockPackaged, blockPackaged.getHash(), blockPackaged, new CertifyOperation(farmer, farmer, wheat.clone().process()), 3);
        Block blockTransferred = new Block(blockCertified, blockCertified.getHash(), blockCertified, new TransferOperation(farmer, reviewer, wheat.clone(), 10), 4);
        Block blockFinished = new Block(blockTransferred, blockTransferred.getHash(), blockTransferred, new FinishOperation(reviewer, reviewer, wheat.clone().process()), 5);

        String report = blockFinished.generateReport();
        System.out.println(report);

    }

}
