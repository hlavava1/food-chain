package FoodChain.blockchain;

import FoodChain.RequestType;
import FoodChain.channel.Channel;
import FoodChain.channel.ChannelFactory;
import FoodChain.entity.Entity;
import FoodChain.Iterator;
import FoodChain.foodStuff.FarmedState;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.Wheat;
import FoodChain.operations.FarmOperation;
import FoodChain.operations.Operation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class containing various tests with respect to Blockchain assuring that the code is valid
 */
public class BlockchainTest {

    private Channel channel;
    private Entity farmer;
    private FoodStuff foodStuff;
    private Blockchain blockchain;
    private Blockchain otherBlockchain;
    private Block genesisBlock;
    private List<Block> blocks;
    private PrivateKey privateKey;
    private PublicKey publicKey;

    @Before
    public void setup() {

        // Create the key pair
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair pair = keyPairGenerator.generateKeyPair();
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }

        ChannelFactory channelFactory = new ChannelFactory();
        channel = channelFactory.makeGrapeChannel(RequestType.DEMAND);
        farmer = new Entity(channel, privateKey, publicKey);
        foodStuff = new Wheat();

        Operation operation = new FarmOperation(farmer, farmer, foodStuff);
        genesisBlock = new Block(null, null, null, operation,0, null);
        genesisBlock.sign(privateKey);
        blocks = new ArrayList<>();

        int numberOfBlocks = 10;
        Block previousBlock, newBlock, previousBlockClone, otherNewBlock = null;

        for (int i = 0; i < numberOfBlocks; i++) {
            previousBlock = i == 0 ? genesisBlock : blocks.get(blocks.size() - 1);
            operation = new FarmOperation(farmer, farmer, foodStuff);

            newBlock = new Block(previousBlock, previousBlock.getHash(), previousBlock, operation, i + 1);
            newBlock.sign(privateKey);

            previousBlockClone = (Block) previousBlock.clone();
            otherNewBlock = new Block(previousBlockClone, previousBlockClone.getHash(), previousBlockClone, operation, i + 1);
            otherNewBlock.sign(privateKey);

            blocks.add(newBlock);
        }

        blockchain = new Blockchain(blocks.get(numberOfBlocks - 1));
        otherBlockchain = new Blockchain(otherNewBlock);
    }

    @Test
    public void testIteratorIteratesProperly() {

        Iterator<Block> blockchainIterator = blockchain.iterator();
        int blockIndex = blocks.size() - 1;

        Block currentBlock;
        while (blockchainIterator.hasNext()) {
            currentBlock = blockchainIterator.next();

            if (blockIndex != -1) {
                Assert.assertEquals(blocks.get(blockIndex), currentBlock);
            } else {
                Assert.assertEquals(genesisBlock, currentBlock);
            }

            blockIndex--;
        }

        Assert.assertEquals(-2, blockIndex);
    }

    @Test(expected = InvalidHashException.class)
    public void testDoesntAcceptBlockWithInvalidHash() throws Exception {
        Operation newOperation = new FarmOperation(farmer, farmer, foodStuff);
        Block newBlock = new Block(
                blocks.get(blocks.size() - 1),
                blocks.get(blocks.size() - 2).getHash(),
                blocks.get(blocks.size() - 1), newOperation, blocks.get(blocks.size() - 1).getTimestamp() + 1);

        newBlock.sign(privateKey);

        otherBlockchain.appendBlock(newBlock);
    }

    @Test
    public void testCorrectlyVerifiesUnmodifiedChain() {
        Assert.assertTrue(blockchain.verify());
    }

    @Test(expected = DoubleSpendingException.class)
    public void testDetectsDoubleSpending() throws Exception {
        Operation newOperation = new FarmOperation(farmer, farmer, foodStuff);
        Block newBlock = new Block(
                blocks.get(blocks.size() - 1),
                blocks.get(blocks.size() - 1).getHash(),
                blocks.get(blocks.size() - 2), newOperation, blocks.get(blocks.size() - 1).getTimestamp() + 1);

        newBlock.sign(privateKey);

        otherBlockchain.appendBlock(newBlock);
    }

    @Test
    public void testDetectsChangesToBlocks() {
        // Change the signature of a random block in the chain
        Random rand = new Random();
        int blockIndex = rand.nextInt(blocks.size());

        blocks.get(blockIndex).setSignature("123".getBytes(StandardCharsets.UTF_8));

        Assert.assertFalse(blockchain.verify());
    }

}
