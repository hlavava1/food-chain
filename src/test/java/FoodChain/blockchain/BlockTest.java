package FoodChain.blockchain;

import FoodChain.RequestType;
import FoodChain.channel.Channel;
import FoodChain.channel.ChannelFactory;
import FoodChain.entity.Entity;
import FoodChain.entity.FarmerEntity;
import FoodChain.foodStuff.FarmedState;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.Wheat;
import FoodChain.operations.FarmOperation;
import FoodChain.operations.Operation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.BadPaddingException;
import java.security.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class containing various tests with respect to Block assuring that the code is valid
 */
public class BlockTest {

    private Channel channel;
    private Entity farmer;
    private FoodStuff foodStuff;
    private Block genesisBlock;
    private List<Block> blocks;
    private PrivateKey privateKey;
    private PublicKey publicKey;
    private PrivateKey foreignPrivateKey;
    private PublicKey foreignPublicKey;

    @Before
    public void setup() {

        // Create the key pair
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(2048);
            KeyPair pair = keyPairGenerator.generateKeyPair();
            privateKey = pair.getPrivate();
            publicKey = pair.getPublic();

            KeyPair foreignPair = keyPairGenerator.generateKeyPair();
            foreignPrivateKey = foreignPair.getPrivate();
            foreignPublicKey = foreignPair.getPublic();
        } catch (NoSuchAlgorithmException exception) {
            System.out.println(exception.getMessage());
            System.exit(1);
        }

        ChannelFactory channelFactory = new ChannelFactory();
        channel = channelFactory.makeGrapeChannel(RequestType.DEMAND);
        farmer = new Entity(channel, privateKey, publicKey);
        foodStuff = new Wheat();

        Operation operation = new FarmOperation(farmer, farmer, foodStuff);

        genesisBlock = new Block(null, null, null, operation,0);
        genesisBlock.sign(privateKey);
        blocks = new ArrayList<>();

        int numberOfBlocks = 10;
        Block previousBlock, newBlock;
        String signature;
        for (int i = 0; i < numberOfBlocks; i++) {
            previousBlock = i == 0 ? genesisBlock : blocks.get(blocks.size() - 1);
            operation = new FarmOperation(farmer, farmer, foodStuff);

            newBlock = new Block(previousBlock, previousBlock.getHash(), previousBlock, operation, i + 1);
            newBlock.sign(privateKey);
            blocks.add(newBlock);

        }

    }

    @Test
    public void testClonesProperly() {
        Block nextBlock = blocks.get(1);

        // Create the clone
        Block nextBlockClone = (Block) nextBlock.clone();

        // Check that all the fields had been copied properly
        Assert.assertEquals(nextBlock.getHash(), nextBlockClone.getHash());
        Assert.assertEquals(nextBlock.getPreviousBlock(), nextBlockClone.getPreviousBlock());
        Assert.assertEquals(nextBlock.getInputBlock(), nextBlockClone.getInputBlock());
        Assert.assertEquals(nextBlock.getOperation(), nextBlockClone.getOperation());
        Assert.assertEquals(nextBlock.getTimestamp(), nextBlockClone.getTimestamp());
        Assert.assertArrayEquals(nextBlock.getSignature(), nextBlockClone.getSignature());

    }

    @Test
    public void testIteratorIteratesProperly() {

        BlockIterator it = (BlockIterator) genesisBlock.iterator();
        Assert.assertTrue(it.hasNext());
        Assert.assertEquals(it.next(), genesisBlock);
        Assert.assertFalse(it.hasNext());

        it = (BlockIterator) blocks.get(blocks.size() - 1).iterator();
        Block nextExpectedBlock = blocks.get(blocks.size() - 1);
        Block nextReceivedBlock;
        while (it.hasNext()) {
            nextReceivedBlock = it.next();
            Assert.assertEquals(nextExpectedBlock, nextReceivedBlock);
            nextExpectedBlock = nextReceivedBlock.getInputBlock();
        }

    }

    @Test
    public void correctlyVerifiesSignature() throws BadPaddingException {
        Random rand = new Random();
        Block block = blocks.get(rand.nextInt(blocks.size()));

        Assert.assertTrue(block.verifySignature(publicKey));
    }

    @Test(expected = BadPaddingException.class)
    public void verificationUsingIncorrectKeyThrowsException() throws BadPaddingException {
        Random rand = new Random();
        Block block = blocks.get(rand.nextInt(blocks.size()));

        Assert.assertFalse(block.verifySignature(foreignPublicKey));
    }

}
