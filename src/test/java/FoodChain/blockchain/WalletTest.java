package FoodChain.blockchain;

import FoodChain.entity.Entity;
import org.javamoney.moneta.Money;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Class containing various tests with respect to Wallet assuring that the code is valid
 */
public class WalletTest {

    Wallet myWallet;

    @Before
    public void setup() {
        myWallet = new Wallet(null, 1000);
    }

    @Test
    public void testInitializesCorrectBalance() {
        Assert.assertEquals(1000.0, myWallet.getMoneyBalance(), 0.001);
    }

    @Test
    public void testReceivesMoney() {
        myWallet.receiveMoney(Money.of(100, "EUR"));
        Assert.assertEquals(1100.0, myWallet.getMoneyBalance(), 0.001);
    }

}
