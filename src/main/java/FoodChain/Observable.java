package FoodChain;

/**
 * Interface for observable objects providing methods to communicate with observers.
 */
public interface Observable {
    void attachListener(Observer observer);
    void detachListener(Observer observer);
    void notifyListeners();
}
