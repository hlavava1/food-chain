package FoodChain.entity;

import FoodChain.channel.Channel;

/**
 * Representation of a distributor.
 */
public class DistributorEntity extends Entity {
    protected DistributorEntity(Channel firstChannel, double money, Strategy strategy) {
        super(firstChannel, money, strategy);
    }
}
