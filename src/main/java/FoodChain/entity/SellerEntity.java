package FoodChain.entity;

import FoodChain.channel.Channel;

/**
 * Representation of a seller.
 */
public class SellerEntity extends Entity {
    protected SellerEntity(Channel firstChannel, double money, Strategy strategy) {
        super(firstChannel, money, strategy);
    }
}
