package FoodChain.entity;

import FoodChain.Observable;
import FoodChain.Request;
import FoodChain.RequestType;
import FoodChain.blockchain.Block;
import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import FoodChain.foodStuff.StateType;
import FoodChain.operations.CleanOperation;
import FoodChain.operations.Operation;
import FoodChain.operations.TransferOperation;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Template for concrete strategies, automatizes partial tasks such as fulfilling accepted requests, demanding more foodstuff,
 * processing and offering foodstuff, accepting requests.
 */
public abstract class Strategy {
    protected StateType toBuy;
    protected StateType toSell;
    protected int moneySpentInSimulationStep;
    public abstract void execute(Entity context, int currentTimestamp);
    public abstract void update(Entity context, Observable observable);

    protected void sendOutRequestedFoodStuff(Entity context, int currentTimestamp) {
        for(Request request : context.toDoList) {
            Map.Entry<FoodStuff, Block> entry = context.getBlockchainClient().getWallet().getFood(request.getFoodType());
            if(entry != null) {
                Operation op = new TransferOperation(context, request.getFrom(), entry.getKey(), request.getPrice());
                context.getBlockchainClient().storeOperation(op, entry.getValue(), currentTimestamp);
                request.setDone();
            }
        }

        context.toDoList = context.toDoList.stream().filter(Predicate.not(Request::isDone)).collect(Collectors.toList());
    }

    protected void demandFoodStuff(Entity context, int minimalStockThreshold) {
        Map<FoodType, Integer> counts = context.getBlockchainClient().getWallet().getAvailableFoodsCount();
        for(FoodType foodType : FoodType.class.getEnumConstants()) {
            Integer ownedCount = context.getBlockchainClient().getWallet().getAvailableFoodsCount().get(foodType);
            if (ownedCount == null) ownedCount = 0;
            if(ownedCount < minimalStockThreshold) {
                Random random = new Random();
                int price = random.nextInt(100) + 1;
                if(price < (context.getBlockchainClient().getWallet().getMoneyBalance() - moneySpentInSimulationStep)) {
                    Request request = new Request(context, RequestType.DEMAND, toBuy, foodType, price);
                    moneySpentInSimulationStep += price;
                    context.firstChannel.receiveRequest(request);
                }
            }
        }
    }

    protected void processOrOfferStock(Entity context, int currentTimestamp) {
        Map<FoodType, Integer> counts = context.getBlockchainClient().getWallet().getAvailableFoodsCount();
        for(Map.Entry<FoodType, Integer> entry : counts.entrySet()) {
            if(entry.getValue() > 0) {
                List<Map.Entry<FoodStuff, Block>> list = context.getBlockchainClient().getWallet().getAllFoodOfType(entry.getKey());
                for(Map.Entry<FoodStuff, Block> foodStuff : list) {
                    if(foodStuff.getKey().getFoodState().getType() == toSell) {
                        Block lastBlock = foodStuff.getValue();
                        while(lastBlock != null && !(lastBlock.getOperation() instanceof TransferOperation)) {
                            lastBlock = lastBlock.getInputBlock();
                        }
                        int price;
                        if(lastBlock != null) {
                            TransferOperation transfer = (TransferOperation)lastBlock.getOperation();
                            price = (int)(transfer.getPrice() * 1.5);
                        } else {
                            Random random = new Random();
                            price = random.nextInt(100) + 1;
                        }
                        Request r = new Request(context, RequestType.OFFER, toSell, entry.getKey(), price);
                        context.offers.add(r);
                        context.firstChannel.receiveRequest(r);
                    } else {
                        processFoodStuff(context, foodStuff, currentTimestamp);
                    }
                }
            }
        }
    }

    /**
     * Method implemented by specific strategies according to type of operation executed on foodstuff.
     * @param context - Entity using strategy
     * @param foodStuff - Foodstuff to process
     * @param currentTimestamp - Step in simulation
     */
    public abstract void processFoodStuff(Entity context, Map.Entry<FoodStuff, Block> foodStuff, int currentTimestamp);

    protected void takeRequests(Entity context, Request request) {
        if(request.getType() == RequestType.DEMAND && request.getStateType() == toSell &&
                context.getBlockchainClient().getWallet().getAllFoodOfType(request.getFoodType()).size() > 0) {
            if(request.accept(context)) {
                context.toDoList.add(request);
            }
        } else if(request.getType() == RequestType.OFFER && request.getStateType() == toBuy &&
                request.getPrice() < (context.getBlockchainClient().getWallet().getMoneyBalance() - moneySpentInSimulationStep)) {
            moneySpentInSimulationStep += request.getPrice();
            request.accept(context);
        }
    }
}
