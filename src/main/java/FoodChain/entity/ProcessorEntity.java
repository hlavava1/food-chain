package FoodChain.entity;

import FoodChain.channel.Channel;

/**
 * Representation of a processor.
 */
public class ProcessorEntity extends Entity {
    protected ProcessorEntity(Channel firstChannel, double money, Strategy strategy) {
        super(firstChannel, money, strategy);
    }
}
