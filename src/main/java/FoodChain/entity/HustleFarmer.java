package FoodChain.entity;

import FoodChain.Observable;
import FoodChain.Request;
import FoodChain.RequestType;
import FoodChain.blockchain.Block;
import FoodChain.channel.Channel;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;
import FoodChain.foodStuff.StateType;
import FoodChain.operations.CleanOperation;
import FoodChain.operations.Operation;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Strategy for farmers with enough stock - focusing on selling as much as possible.
 */
public class HustleFarmer extends Strategy {

    protected HustleFarmer(StateType toSell){
        this.toBuy = null;
        this.toSell = toSell;
    }

    /**
     * Automatizes actions of farmer with enough stock - fulfill requests, offer redundant foodstuff, switch strategy
     * back to productive if stock's running low.
     * @param context - Entity using strategy
     * @param currentTimestamp - Step in simulation
     */
    @Override
    public void execute(Entity context, int currentTimestamp) {
        sendOutRequestedFoodStuff(context, currentTimestamp);

        processOrOfferStock(context, currentTimestamp);

        if(toSell == StateType.RAW && context.getBlockchainClient().getWallet().getAllFoodOfType(FoodType.MEAT).size() < 2) {
            context.setStrategy(new ProductiveMeatFarmer());
        } else {
            for(FoodType foodType : FoodType.class.getEnumConstants()) {
                if(context.getBlockchainClient().getWallet().getAllFoodOfType(foodType).size() > 1) {
                    return;
                }
            }
            context.setStrategy(new ProductiveVegFarmer());
        }
    }

    /**
     * Accepts requests according to strategy settings.
     * @param context - Entity using strategy
     * @param channel - Observable channel object
     */
    @Override
    public void update(Entity context, Observable channel) {
        takeRequests(context, ((Channel)channel).getLastRequest());
    }

    /**
     * Does nothing - farmer only generates or sells raw/farmed foodstuff - no processing required from him.
     * @param context - Entity processing the food
     * @param foodStuff - Food stuff to process
     * @param currentTimestamp - simulation's timestamp
     */
    @Override
    public void processFoodStuff(Entity context, Map.Entry<FoodStuff, Block> foodStuff, int currentTimestamp) {

    }
}
