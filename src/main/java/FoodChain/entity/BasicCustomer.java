package FoodChain.entity;

import FoodChain.Observable;
import FoodChain.blockchain.Block;
import FoodChain.channel.Channel;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.StateType;
import FoodChain.operations.Operation;
import FoodChain.operations.PackageOperation;
import org.javamoney.moneta.Money;

import java.util.Map;

/**
 * Basic strategy for customers - demand any foodstuff if on a low stock.
 */
public class BasicCustomer extends Strategy {
    protected BasicCustomer() {
        this.toBuy = StateType.FINAL;
        this.toSell = null;
    }

    /**
     * Automatizes customer actions - earn monthly income and try not to run out of food.
     * @param context - Entity using strategy
     * @param currentTimestamp - Step in simulation
     */
    @Override
    public void execute(Entity context, int currentTimestamp) {
        if(currentTimestamp % 30 == 0) {
            context.getBlockchainClient().getWallet().receiveMoney(Money.of(1000, "EUR"));
        }
        demandFoodStuff(context, 3);
    }

    /**
     * Accepts requests according to strategy settings.
     * @param context - Entity using strategy
     * @param channel - Observable channel object
     */
    @Override
    public void update(Entity context, Observable channel) {
        takeRequests(context, ((Channel)channel).getLastRequest());
    }

    /**
     * Does nothing - no need for customer to further process foodstuff.
     * @param context - Entity processing the food
     * @param foodStuff - Food stuff to process
     * @param currentTimestamp - simulation's timestamp
     */
    @Override
    public void processFoodStuff(Entity context, Map.Entry<FoodStuff, Block> foodStuff, int currentTimestamp) {

    }
}
