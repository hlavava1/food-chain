package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Represents the finish operation
 */
public class FinishOperation extends Operation {

    /**
     * FinishOperation constructor
     * @param from operation performer
     * @param to entity that owns the product after operation
     * @param foodStuff product on which the operation has been performed
     */
    public FinishOperation(Entity from, Entity to, FoodStuff foodStuff) {
        super(from, to, foodStuff);
    }

    @Override
    public String toString() {
        return "Finally reviewed by " + from.toString();
    }
}
