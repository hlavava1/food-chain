package FoodChain.operations;

import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodStuff;

/**
 * Represents the gather operation
 */
public class GatherOperation extends Operation {

    /**
     * GatheOperation constructor
     * @param from operation performer
     * @param to entity that owns the product after operation
     * @param foodStuff product on which the operation has been performed
     */
    public GatherOperation(Entity from, Entity to, FoodStuff foodStuff) {
        super(from, to, foodStuff);
    }

    @Override
    public String toString() {
        return "Gathered by " + from.toString();
    }
}
