package FoodChain.blockchain;

/**
 * An exception thrown when a digital signature has been
 */
public class InvalidSignature extends Exception {
    public InvalidSignature() { super("EXCEPTION: Invalid signature"); };
    public InvalidSignature(String message) { super(message); };
}
