package FoodChain.blockchain;

import FoodChain.Iterator;

/**
 * Provides an iterator over blocks that store the information about processing a certain product
 */
public class BlockIterator implements Iterator<Block> {

    private Block currentBlock;

    /**
     * Constructs the BlockIterator
     * @param startingBlock block from which the iteration continues further
     */
    public BlockIterator(Block startingBlock) {
        this.currentBlock = startingBlock;
    }

    /**
     * Checks whether there are any elements to iterate over
     * @return true when there are elements remaining, false otherwise
     */
    @Override
    public boolean hasNext() {
        return currentBlock != null;
    }

    /**
     * Returns the next element to iterate over
     * @return next element if present, null otherwise
     */
    @Override
    public Block next() {
        if (!hasNext()) return null;

        Block requestedBlock = currentBlock;
        currentBlock = currentBlock.getInputBlock();

        return requestedBlock;
    }
}
