package FoodChain.blockchain;

import FoodChain.Iterator;

/**
 * Class used to iterate over the whole Blockchain, yielding Blockchain blocks
 * in order from last to the first
 */
public class BlockchainIterator implements Iterator<Block> {

    private Block currentBlock;

    BlockchainIterator(Block tail) {
        this.currentBlock = tail;
    }

    /**
     * Checks whether there are any elements to iterate over
     * @return true when there are elements remaining, false otherwise
     */
    @Override
    public boolean hasNext() {
        return currentBlock != null;
    }

    /**
     * Returns the next element to iterate over
     * @return next element if present, null otherwise
     */
    @Override
    public Block next() {
        if (!hasNext()) return null;

        Block requestedBlock = currentBlock;
        currentBlock = currentBlock.getPreviousBlock();

        return requestedBlock;
    }
}
