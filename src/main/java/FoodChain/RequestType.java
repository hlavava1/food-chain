package FoodChain;

/**
 * Possible types of requests.
 */
public enum RequestType {
    DEMAND,
    OFFER;
}
