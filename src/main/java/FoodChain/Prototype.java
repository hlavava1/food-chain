package FoodChain;


/**
 * An interface providing the ability to clone an object
 */
public interface Prototype {
    /**
     * Return clone of the object
     * @return clone of the object
     */
    Prototype clone();

}
