package FoodChain.channel;

import FoodChain.RequestType;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;

/**
 * Representation of a channel dedicated to potato requests
 */
public class PotatoChannel extends Channel {
    protected PotatoChannel(RequestType type) {
        super();
        this.acceptedFoodType = FoodType.POTATO;
        this.acceptedRequestType = type;
    }
}
