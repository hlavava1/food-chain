package FoodChain.channel;

import FoodChain.RequestType;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;

/**
 * Representation of a channel dedicated to tomato requests
 */
public class TomatoChannel extends Channel {
    protected TomatoChannel(RequestType type) {
        super();
        this.acceptedFoodType = FoodType.TOMATO;
        this.acceptedRequestType = type;
    }
}
