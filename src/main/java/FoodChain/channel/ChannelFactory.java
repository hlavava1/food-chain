package FoodChain.channel;

import FoodChain.Request;
import FoodChain.RequestType;

/**
 * Simple factory for channels, channel management included.
 */
public class ChannelFactory {
    private Channel firstChannel;
    private Channel lastChannel;

    /**
     * Channel factory constructor
     */
    public ChannelFactory() {
        firstChannel = null;
        lastChannel = null;
    }

    /**
     * Gets a channel responsible for accepting requests.
     * @return First channel
     */
    public Channel getFirstChannel() {
        return this.firstChannel;
    }

    private void updateChannelList(Channel ch) {
        if (firstChannel == null) {
            firstChannel = ch;
        } else {
            lastChannel.nextHandler = ch;
        }
        lastChannel = ch;
    }

    /**
     * Creates a channel dedicated to grape requests.
     * @param type - DEMAND/OFFER
     * @return Grape channel instance
     */
    public Channel makeGrapeChannel(RequestType type) {
       Channel channel = new GrapeChannel(type);
       updateChannelList(channel);
       return channel;
    }

    /**
     * Creates a channel dedicated to meat requests.
     * @param type - DEMAND/OFFER
     * @return Meat channel instance
     */
    public Channel makeMeatChannel(RequestType type) {
        Channel channel = new MeatChannel(type);
        updateChannelList(channel);
        return channel;
    }

    /**
     * Creates a channel dedicated to potato requests.
     * @param type - DEMAND/OFFER
     * @return Potato channel instance
     */
    public Channel makePotatoChannel(RequestType type) {
        Channel channel = new PotatoChannel(type);
        updateChannelList(channel);
        return channel;
    }

    /**
     * Creates a channel dedicated to tomato requests.
     * @param type - DEMAND/OFFER
     * @return Tomato channel instance
     */
    public Channel makeTomatoChannel(RequestType type) {
        Channel channel = new TomatoChannel(type);
        updateChannelList(channel);
        return channel;
    }

    /**
     * Creates a channel dedicated to wheat requests.
     * @param type - DEMAND/OFFER
     * @return Wheat channel instance
     */
    public Channel makeWheatChannel(RequestType type) {
        Channel channel = new WheatChannel(type);
        updateChannelList(channel);
        return channel;
    }
}
