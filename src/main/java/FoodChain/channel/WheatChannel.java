package FoodChain.channel;

import FoodChain.RequestType;
import FoodChain.foodStuff.FoodStuff;
import FoodChain.foodStuff.FoodType;

/**
 * Representation of a channel dedicated to wheat requests
 */
public class WheatChannel extends Channel {
    protected WheatChannel(RequestType type) {
        super();
        this.acceptedFoodType = FoodType.WHEAT;
        this.acceptedRequestType = type;
    }
}
