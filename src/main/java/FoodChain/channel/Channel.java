package FoodChain.channel;

import FoodChain.Observable;
import FoodChain.Observer;
import FoodChain.Request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Abstract representation of channel implementing Observable interface and extending Handler (chain of responsibility)
 */
public abstract class Channel extends Handler implements Observable {
    protected List<Observer> listeners;
    protected List<Request> requestsList;

    protected Channel() {
        nextHandler = null;
        listeners = new ArrayList<>();
        requestsList = new ArrayList<>();
    }

    /**
     * Attaches observer to listen to channel.
     * @param obs - Observer to observe channel
     */
    @Override
    public void attachListener(Observer obs) {
        listeners.add(obs);
    }

    /**
     * Detaches observer from listening to channel.
     * @param obs - Observer to detach from channel
     */
    @Override
    public void detachListener(Observer obs) {
        listeners.remove(obs);
    }

    /**
     * Notify listeners about change in observable object
     */
    @Override
    public void notifyListeners() {

        Collections.shuffle(listeners);

        for(Observer obs : listeners) {
            obs.update(this);
        }
    }

    @Override
    protected void handleRequest(Request req) {
        requestsList.add(req);
        notifyListeners();
    }

    /**
     * Provide last registered request in a channel
     * @return Last request in channel
     */
    public Request getLastRequest() {
        return requestsList.get(requestsList.size() - 1);
    }
}
