package FoodChain.foodStuff;

import FoodChain.Prototype;

/**
 * Tomato class
 */
public class Tomato extends FoodStuff implements Prototype {

    /**
     * Creates a new Tomato with its predefined initial state
     */
    public Tomato() {
        super(new FarmedState());
    }

    private Tomato(FoodState initialState) {
        super(initialState);
    }

    @Override
    public FoodType getType() {
        return FoodType.TOMATO;
    }

    @Override
    public String getName() {
        return "Tomato";
    }

    @Override
    public Tomato clone() {
        return new Tomato(state);
    }
}
