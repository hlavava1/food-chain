package FoodChain.foodStuff;

/**
 * Initial farmed state that is used only for crops
 */
public class FarmedState implements FoodState {
    @Override
    public void process(FoodStuff context) {

        FoodType type = context.getType();

        switch (type) {
            case GRAPE:
            case POTATO:
            case TOMATO:
            case WHEAT:
                context.setState(new CleanedState());
                break;
        }

    }

    @Override
    public String toString() {
        return "farmed";
    }

    @Override
    public StateType getType() { return StateType.FARMED; }
}
