package FoodChain.foodStuff;

import FoodChain.Prototype;

/**
 * Meat class
 */
public class Meat extends FoodStuff implements Prototype {

    /**
     * Creates a new Meat with its predefined initial state
     */
    public Meat() {
        super(new RawState());
    }

    private Meat(FoodState initialState) {
        super(initialState);
    }

    @Override
    public FoodType getType() {
        return FoodType.MEAT;
    }

    @Override
    public String getName() {
        return "Meat";
    }

    @Override
    public Meat clone() {
        return new Meat(state);
    }
}
