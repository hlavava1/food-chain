package Simulation;

import FoodChain.blockchain.BlockchainDistributor;
import FoodChain.entity.Entity;
import FoodChain.foodStuff.FoodType;
import io.chucknorris.client.ChuckNorrisClient;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Abstract class used to provide the basic functions for every simulation
 */
public abstract class Simulation {

    protected int timestamp;
    protected List<Entity> entities;
    protected StringBuilder transactionReport;
    protected ChuckNorrisClient chuckNorris;


    public Simulation() {
        entities = new ArrayList<>();
        transactionReport = new StringBuilder();
        chuckNorris = new ChuckNorrisClient();
    }

    /**
     * Run the simulation for specified steps count
     * @param iterationsCount number of steps the simulation should perform
     */
    public void run(int iterationsCount) {
        initialSetup();

        for (int i = 0; i < iterationsCount; i++) {
            runIteration();

            System.out.println("Step number: " + i);
            System.out.println("To keep you entertained, here's a Chuck Norris joke:");
            System.out.println(chuckNorris.getRandomJoke().getValue()); // Inovace
            System.out.println();

            transactionReport.append("=== STEP ").append(timestamp).append(" ===\n");
            for (Entity entity : entities) {
                transactionReport.append(entity.toString()).append("\n");
                Map.Entry<Double, Map<FoodType, Integer>> walletReport = entity.generateReport();

                transactionReport.append("Money balance: ").append(walletReport.getKey()).append("\n");

                transactionReport.append("Owned food:\n");
                if (walletReport.getValue().isEmpty()) {
                    transactionReport.append("- None\n");
                } else {
                    for (Map.Entry<FoodType, Integer> entry : walletReport.getValue().entrySet()) {
                        transactionReport.append("- ").append(entry.getKey().toString()).append(": ").append(entry.getValue()).append("\n");
                    }
                }

                transactionReport.append(entity.generateTransactionsReport());
                transactionReport.append("\n\n");
            }
        }
    }

    protected void initialSetup() {
        timestamp = 0;
        BlockchainDistributor.getInstance().clearDoubleSpendingAttempts();
        createChannels();
        createEntities();
    }

    protected abstract void createEntities();
    protected abstract void createChannels();

    protected void runIteration() {
        for (Entity entity : entities) {
            entity.act(timestamp);
        }

        timestamp++;
    }

    /**
     * Stores the transaction report into a file with specified name
     * @param filename name of the output file
     */
    public void generateTransactionReport(String filename) {
        try {
            FileWriter writer = new FileWriter(filename);
            writer.write(transactionReport.toString());
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Stores the security report into a file with specified name
     * @param filename name of the output file
     */
    public void generateSecurityReport(String filename) {
        try {
            FileWriter writer = new FileWriter(filename);
            writer.write(BlockchainDistributor.getInstance().generateSecurityReport());
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
