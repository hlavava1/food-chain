import Simulation.*;

public class Main {

    public static void main(String[] args) {

        Simulation simulation;

        System.out.println("Starting basic simulation configuration");
        simulation = new BasicSimulation();
        simulation.run(10);
        simulation.generateTransactionReport("basicSimulationTransactionReport.txt");
        simulation.generateSecurityReport("basicSimulationSecurityReport.txt");
        System.out.println("Basic simulation ended");


        System.out.println("Starting the not so basic simulation configuration");
        simulation = new NotSoBasicSimulation();
        simulation.run(10);
        simulation.generateTransactionReport("notSoBasicSimulationTransactionReport.txt");
        simulation.generateSecurityReport("notSoBasicSimulationSecurityReport.txt");
        System.out.println("Not So Basic simulation ended");

    }

}
